public class Processor {
    private String processorBrand;
    private String processorModel;
    private int processorPrice;

    public Processor() {
    }

    public Processor(String processorBrand, String processorModel, int processorPrice) {
        this.processorBrand = processorBrand;
        this.processorModel = processorModel;
        this.processorPrice = processorPrice;
    }

    public String getProcessorBrand() {
        return processorBrand;
    }

    public void setProcessorBrand(int option) {
        switch (option) {
            case 1:
                this.processorBrand = "Intel";
                break;
            case 2:
                this.processorBrand = "AMD";
                break;
            default:
                System.out.println("Chose a valid option");
                this.processorBrand = "Invalid";
                break;
        }
    }

    public String getProcessorModel() {
        return processorModel;
    }

    public void setProcessorModel(int option) {
        if (this.processorBrand.equals("Intel")) {
            switch (option) {
                case 1:
                    this.processorModel = "I3 3000";
                    break;
                case 2:
                    this.processorModel = "I5 5000";
                    break;
                case 3:
                    this.processorModel = "I7 7000";
                    break;
                case 4:
                    this.processorModel = "I9 9000";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.processorModel = "Invalid";
                    break;
            }
        } else if (this.processorBrand.equals("AMD")) {
            switch (option) {
                case 1:
                    this.processorModel = "Ryzen 2000";
                    break;
                case 2:
                    this.processorModel = "Ryzen 3000";
                    break;
                case 3:
                    this.processorModel = "Ryzen 4000";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.processorModel = "Invalid";
                    break;
            }
        } else {
            System.out.println("You need to chose a valid brand before you chose a model");
        }
    }

    public int getProcessorPrice() {
        return processorPrice;
    }

    public void setProcessorPrice() {
        if (this.processorModel.equals("I3 3000") || this.processorModel.equals("Ryzen 2000")) {
            this.processorPrice = 500;
        } else if (this.processorModel.equals("I5 5000") || this.processorModel.equals("Ryzen 3000")) {
            this.processorPrice = 700;
        } else if (this.processorModel.equals("I7 7000") || this.processorModel.equals("Ryzen 4000")) {
            this.processorPrice = 900;
        } else if (this.processorModel.equals("I9 9000")) {
            this.processorPrice = 1000;
        }
    }
}
