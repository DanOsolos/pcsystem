public class PowerSupply {
    private int powerSupplyPower;
    private String powerSupplyBrand;
    private String powerSupplyModel;
    private int powerSupplyPrice;

    public PowerSupply() {
    }

    public PowerSupply(int powerSupplyPower, String powerSupplyBrand, String powerSupplyModel, int powerSupplyPrice) {
        this.powerSupplyBrand = powerSupplyBrand;
        this.powerSupplyModel = powerSupplyModel;
        this.powerSupplyPower = powerSupplyPower;
        this.powerSupplyPrice = powerSupplyPrice;
    }

    public int getPowerSupplyPower() {
        return powerSupplyPower;
    }

    public void setPowerSupplyPower(int option) {
        switch (option) {
            case 1:
                this.powerSupplyPower = 750;
                break;
            case 2:
                this.powerSupplyPower = 1000;
                break;
            case 3:
                this.powerSupplyPower = 1500;
                break;
            default:
                System.out.println("Enter a valid option");
                this.powerSupplyPower = 0;
        }
    }

    public String getPowerSupplyBrand() {
        return powerSupplyBrand;
    }

    public void setPowerSupplyBrand(int option) {
        switch (option) {
            case 1:
                this.powerSupplyBrand = "ASUS";
                break;
            case 2:
                this.powerSupplyBrand = "Corsair";
                break;
            case 3:
                this.powerSupplyBrand = "GIGABYTE";
                break;
            default:
                System.out.println("Enter a valid option");
                this.powerSupplyBrand = "Invalid";
        }
    }

    public String getPowerSupplyModel() {
        return powerSupplyModel;
    }

    public void setPowerSupplyModel(int option) {
        switch (this.powerSupplyPower) {
            case 750:
                switch (option) {
                    case 1:
                        this.powerSupplyModel = "Model 1 750W";
                        break;
                    case 2:
                        this.powerSupplyModel = "Model 2 750W";
                        break;
                    case 3:
                        this.powerSupplyModel = "Model 3 750W";
                        break;
                    default:
                        this.powerSupplyModel = "Invalid";
                        break;
                }
                break;
            case 1000:
                switch (option) {
                    case 1:
                        this.powerSupplyModel = "Model 1 1000W";
                        break;
                    case 2:
                        this.powerSupplyModel = "Model 2 1000W";
                        break;
                    case 3:
                        this.powerSupplyModel = "Model 3 1000W";
                        break;
                    default:
                        this.powerSupplyModel = "Invalid";
                        break;
                }
                break;
            case 1500:
                switch (option) {
                    case 1:
                        this.powerSupplyModel = "Model 1 1500W";
                        break;
                    case 2:
                        this.powerSupplyModel = "Model 2 1500W";
                        break;
                    case 3:
                        this.powerSupplyModel = "Model 3 1500W";
                        break;
                    default:
                        this.powerSupplyModel = "Invalid";
                        break;
                }
                break;
            default:
                this.powerSupplyModel = "Invalid power";
                break;
        }
    }

    public int getPowerSupplyPrice() {
        return powerSupplyPrice;
    }

    public void setPowerSupplyPrice() {
        if (this.powerSupplyModel.contains("Model 1")){
            this.powerSupplyPrice = 100;
        } else if (this.powerSupplyModel.contains("Model 2")){
            this.powerSupplyPrice = 200;
        } else if (this.powerSupplyModel.contains("Model 3")){
            this.powerSupplyPrice = 300;
        }
    }
}
