public class Keyboard {
    private String keyboardBrand;
    private boolean mechanical;
    private int keyboardPrice;

    public Keyboard() {
    }

    public Keyboard(String keyboardBrand, boolean mechanical, int keyboardPrice) {
        this.keyboardBrand = keyboardBrand;
        this.keyboardPrice = keyboardPrice;
        this.mechanical = mechanical;
    }

    public String getKeyboardBrand() {
        return keyboardBrand;
    }

    public void setKeyboardBrand(int option) {
        switch (option) {
            case 1:
                this.keyboardBrand = "Corsair";
                break;
            case 2:
                this.keyboardBrand = "HyperX";
                break;
            default:
                System.out.println("Enter a valid option");
                this.keyboardBrand = "Invalid";
                break;
        }
    }

    public boolean isMechanical() {
        return mechanical;
    }

    public void setMechanical(int option) {
        switch (option) {
            case 1:
                this.mechanical = true;
                break;
            case 2:
                this.mechanical = false;
                break;
            default:
                System.out.println("Enter a valid option");
                this.mechanical = false;
                break;
        }
    }

    public int getKeyboardPrice() {
        return keyboardPrice;
    }

    public void setKeyboardPrice() {
        if(this.keyboardBrand.equals("Corsair")){
            this.keyboardPrice = 20;
        } else  if(this.keyboardBrand.equals("HyperX")){
            this.keyboardPrice = 30;
        }
    }
}
