public class Mouse {

    private String mouseBrand;
    private boolean optical;
    private int mousePrice;

    public Mouse() {
    }

    public Mouse(String mouseBrand, boolean optical, int mousePrice) {
        this.mouseBrand = mouseBrand;
        this.optical = optical;
        this.mousePrice = mousePrice;
    }

    public String getMouseBrand() {
        return mouseBrand;
    }

    public void setMouseBrand(int option) {
        switch (option) {
            case 1:
                this.mouseBrand = "SteelSeries";
                break;
            case 2:
                this.mouseBrand = "Razor";
                break;
            default:
                System.out.println("Enter a valid option");
                this.mouseBrand = "Invalid";
                break;
        }
    }

    public boolean isOptical() {
        return optical;
    }

    public void setOptical(int option) {
        switch (option) {
            case 1:
                this.optical = true;
                break;
            case 2:
                this.optical = false;
                break;
            default:
                System.out.println("Enter a valid option");
                this.optical = false;
        }
    }

    public int getMousePrice() {
        return mousePrice;
    }

    public void setMousePrice() {
        if(this.mouseBrand.equals("SteelSeries")){
            this.mousePrice = 15;
        } else if (this.mouseBrand.equals("Razor")){
            this.mousePrice = 20;
        }
    }
}
