public class Motherboard {
    private String motherboardBrand;
    private String motherboardSocket;
    private String motherBoardModel;
    private VideoCard videoCard;
    private RAM RAM;
    private Processor processor;
    private int motherBordPrice;

    public Motherboard() {
    }

    public Motherboard(int motherBordPrice, String motherboardBrand, String motherboardSocket, String motherBoardModel, VideoCard videoCard, RAM RAM, Processor processor) {
        this.motherBoardModel = motherBoardModel;
        this.motherboardSocket = motherboardSocket;
        this.motherboardBrand = motherboardBrand;
        this.videoCard = videoCard;
        this.RAM = RAM;
        this.processor = processor;
        this.motherBordPrice = motherBordPrice;
    }


    public String getMotherboardBrand() {
        return motherboardBrand;
    }

    public void setMotherboardBrand(int option) {
        switch (option) {
            case 1:
                this.motherboardBrand = "ASUS";
                break;
            case 2:
                this.motherboardBrand = "MSI";
                break;
            case 3:
                this.motherboardBrand = "GIGABYTE";
                break;
            default:
                System.out.println("Enter a valid option");
                this.motherboardBrand = "Invalid brand";
        }
    }

    public String getMotherboardSocket() {
        return motherboardSocket;
    }

    public void setMotherboardSocket(int option) {
        switch (option) {
            case 1:
                this.motherboardSocket = "Intel";
                break;
            case 2:
                this.motherboardSocket = "AMD";
                break;
            default:
                System.out.println("Enter a valid option");
                this.motherboardSocket = "Invalid socket";
                break;
        }
    }

    public String getMotherBoardModel() {
        return motherBoardModel;
    }

    public void setMotherBoardModel(int option) {
        if (this.motherboardSocket.equals("Intel")) {
            switch (option) {
                case 1:
                    this.motherBoardModel = "Model 1";
                    break;
                case 2:
                    this.motherBoardModel = "Model 2";
                    break;
                case 3:
                    this.motherBoardModel = "Model 3";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.motherBoardModel = "Invalid";
                    break;
            }
        } else if (this.motherboardSocket.equals("AMD")) {
            switch (option) {
                case 1:
                    this.motherBoardModel = "Model 1";
                    break;
                case 2:
                    this.motherBoardModel = "Model 2";
                    break;
                case 3:
                    this.motherBoardModel = "Model 3";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.motherBoardModel = "Invalid";
                    break;
            }
        } else {
            System.out.println("You need to chose a valid socket before you chose a model");
        }
    }

    public VideoCard getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(VideoCard videoCard) {
        this.videoCard = videoCard;
    }

    public RAM getRAM() {
        return RAM;
    }

    public void setRAM(RAM RAM) {
        this.RAM = RAM;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public int getMotherBordPrice() {
        return motherBordPrice;
    }

    public void setMotherBordPrice() {
        if (this.motherBoardModel.contains("Model 1")){
            this.motherBordPrice = 50;
        } else if (this.motherBoardModel.contains("Model 2")){
            this.motherBordPrice = 100;
        } else if (this.motherBoardModel.contains("Model 3")){
            this.motherBordPrice = 150;
        }
    }
}

