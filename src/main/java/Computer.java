public class Computer {

    private PcCase pcCase;
    private Motherboard motherboard;
    private Mouse mouse;
    private Monitor monitor;

    public Computer() {
    }

    public Computer(PcCase pcCase, Motherboard motherboard, Mouse mouse, Monitor monitor) {
        this.pcCase = pcCase;
        this.motherboard = motherboard;
        this.mouse = mouse;
        this.monitor = monitor;
    }

    public Mouse getMouse() {
        return mouse;
    }

    public void setMouse(Mouse mouse) {
        this.mouse = mouse;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public PcCase getPcCase() { return pcCase; }

    public void setPcCase(PcCase pcCase) { this.pcCase = pcCase; }

    public Motherboard getMotherboard() { return motherboard; }

    public void setMotherboard(Motherboard motherboard) { this.motherboard = motherboard; }

}
