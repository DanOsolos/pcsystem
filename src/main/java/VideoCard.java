public class VideoCard {
    private String videoCardBrand;
    private String videoCardModel;
    private int videoCardPrice;

    public VideoCard (){}

    public VideoCard (String videoCardBrand, String videoCardModel, int videoCardPrice){
        this.videoCardPrice = videoCardPrice;
        this.videoCardModel = videoCardModel;
        this.videoCardBrand = videoCardBrand;
    }

    public String getVideoCardBrand() {
        return videoCardBrand;
    }

    public void setVideoCardBrand(int option) {
        switch (option) {
            case 1:
                this.videoCardBrand = "Nvidia";
                break;
            case 2:
                this.videoCardBrand = "AMD";
                break;
            default:
                System.out.println("Enter a valid option");
                this.videoCardBrand = "Invalid";
        }
    }

    public String getVideoCardModel() {
        return videoCardModel;
    }

    public void setVideoCardModel(int option) {
        if (this.videoCardBrand.equals("Nvidia")) {
            switch (option) {
                case 1:
                    this.videoCardModel = "GForce 1080";
                    break;
                case 2:
                    this.videoCardModel = "GForce 1080 Ti";
                    break;
                case 3:
                    this.videoCardModel = "RTX 2080";
                    break;
                case 4:
                    this.videoCardModel = "RTX 2080 Ti";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.videoCardModel = "Invalid";
                    break;
            }
        } else if (this.videoCardBrand.equals("AMD")) {
            switch (option) {
                case 1:
                    this.videoCardModel = "Radeon RX 5700";
                    break;
                case 2:
                    this.videoCardModel = "Radeon RX Vega Ti";
                    break;
                case 3:
                    this.videoCardModel = "Radeon RX 5700 XT";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.videoCardModel = "Invalid";
                    break;
            }
        } else {
            System.out.println("You need to chose a valid brand before you chose a model");
        }

    }

    public int getVideoCardPrice() {
        return videoCardPrice;
    }

    public void setVideoCardPrice() {
        if(this.videoCardModel.equals("GForce 1080") || this.videoCardModel.equals("Radeon RX 5700")){
            this.videoCardPrice = 1000;
        } else  if(this.videoCardModel.equals("GForce 1080 Ti") || this.videoCardModel.equals("Radeon RX Vega Ti")){
            this.videoCardPrice = 1300;
        } else  if(this.videoCardModel.equals("GForce 2080") || this.videoCardModel.equals("Radeon RX 5700 XT")){
            this.videoCardPrice = 1600;
        } else  if(this.videoCardModel.equals("GForce 2080 Ti")){
            this.videoCardPrice = 2000;
        }
    }
}
