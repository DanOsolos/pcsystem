public class PcCase {
    private String caseBrand;
    private int casePrice;
    private Motherboard motherboard;
    private PowerSupply powerSupply;

    public PcCase() {}
    public PcCase(Motherboard motherboard, PowerSupply powerSupply, String caseBrand, int casePrice) {
        this.motherboard = motherboard;
        this.powerSupply = powerSupply;
        this.caseBrand = caseBrand;
        this.casePrice = casePrice;
    }

    public String getCaseBrand() {
        return caseBrand;
    }

    public void setCaseBrand(int option) {
        switch (option){
            case 1:
                this.caseBrand = "LG";
                break;
            case 2:
                this.caseBrand = "Samsung";
                break;
            case 3:
                this.caseBrand = "Asus";
                break;
            default:
                System.out.println("Enter a valid option");
                this.caseBrand = "Invalid";
        }
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public PowerSupply getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(PowerSupply powerSupply) {
        this.powerSupply = powerSupply;
    }

    public int getCasePrice() {
        return casePrice;
    }

    public void setCasePrice() {
        if (this.caseBrand.equals("LG")){
            this.casePrice = 100;
        } else if (this.caseBrand.equals("Samsung")){
            this.casePrice = 200;
        } else if (this.caseBrand.equals("Asus")){
            this.casePrice = 300;
        }
    }
}
