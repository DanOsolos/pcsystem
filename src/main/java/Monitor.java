public class Monitor {

    private String monitorBrand;
    private int monitorSize;
    private int monitorPrice;

    public Monitor() {

    }

    public Monitor(String monitorBrand, int displaySize, int monitorPrice) {
        this.monitorBrand = monitorBrand;
        this.monitorSize = displaySize;
        this.monitorPrice = monitorPrice;
    }

    public int getMonitorSize() {
        return monitorSize;
    }

    public void setMonitorSize(int option) {
        switch (option){
            case 1:
                this.monitorSize = 20;
                break;
            case 2:
                this.monitorSize = 25;
                break;
            default:
                System.out.println("Chose a valid option");
                this.monitorBrand = "Invalid";
                break;
        }
    }

    public int getMonitorPrice() {
        return monitorPrice;
    }

    public void setMonitorPrice() {
       if(this.monitorSize == 20){
           this.monitorPrice = 200;
       } else if (this.monitorSize == 25){
           this.monitorPrice = 250;
       }
    }

    public String getMonitorBrand() {
        return monitorBrand;
    }

    public void setMonitorBrand(int option) {
        switch (option){
            case 1:
                this.monitorBrand = "LG";
                break;
            case 2:
                this.monitorBrand = "Samsung";
                break;
            default:
                System.out.println("Chose a valid option");
                this.monitorBrand = "Invalid";
                break;
        }
    }
}
