public class ShowOptions {

    public static void motherboardBrands() {
        System.out.println("Chose motherboard brand:\n1.ASUS\n2.MSI\n3.Gigabyte\nChose and option: ");
    }

    public static void motherboardSocket() {
        System.out.println("Chose motherboard socket type:\n1.Intel\n2.AMD\nChose and option: ");
    }

    public static void motherboardModelIntel() {
        System.out.println("Chose a model for INTEL motherboards:\n1.Model 1\n2.Model 2\n3.Model 3");
    }

    public static void motherboardModelAMD() {
        System.out.println("Chose a model for AMD motherboards:\n1.Model 1\n2.Model 2\n3.Model 3");
    }

    public static void powerSupplyBrand() {
        System.out.println("Chose brand for the power supply:\n1.ASUS\n2.Corsair\n3.Gigabyte\nChose and option: ");
    }

    public static void powerSupplyPower() {
        System.out.println("Chose power level in Watts:\n1.750W\n2.1000W\n3.1500W\nChose and option: ");
    }

    public static void powerSupplyModel750W() {
        System.out.println("Chose a model for 750W:\n1.Model 1 750W\n2.Model 2 750W\n3.Model 3 750W");
    }

    public static void powerSupplyModel1000W() {
        System.out.println("Chose a model for 1000W:\n1.Model 1 1000W\n2.Model 2 1000W\n3.Model 3 1000W");
    }

    public static void powerSupplyModel1500W() {
        System.out.println("Chose a model for 1500W:\n1.Model 1 1500W\n2.Model 2 1500W\n3.Model 3 1500W");
    }

    public static void pcCaseBrand() {
        System.out.println("Chose a brand for pc case:\n1.LG\n2.Samsung\n3.ASUS");
    }

    public static void videoCardBrand() {
        System.out.println("Chose a brand for:\n1.Nvidia\n2.AMD");
    }

    public static void videoCardModelNvidia() {
        System.out.println("Chose a model:\n1.GForce 1080\n2. GForce 1080 Ti\n3.RTX 2080\n4.RTX 2080 Ti");
    }

    public static void videoCardModelAMD() {
        System.out.println("Chose a model:\n1.Radeon RX 5700\n2.Radeon RX Vega Ti\nRadeon RX 5700 XT");
    }

    public static void RAMBrand() {
        System.out.println("Chose a RAM brand:\n1.Corsair\n2.G.Skill");
    }

    public static void RAMModelCorsair() {
        System.out.println("Chose a model for Corsair:\n1.Vengeance LED\n2.Dominator Platinum RGB");
    }

    public static void RAMModelGSkill() {
        System.out.println("Chose a model for G.Skill:\n1.Trident Z RGB\n2.Trident Z Royal");
    }

    public static void RAMAmount() {
        System.out.println("Chose an amount (in megaBytes):\n1.8192\n2.16384\n3.32768");
    }

    public static void processorBrand() {
        System.out.println("Chose a processor brand:\n1.Intel\n2.AMD");
    }

    public static void processorModelIntel() {
        System.out.println("Chose a model for Intel processor:\n1.I3\n2.I5\n3.I7\n4.I9");
    }

    public static void processorModelAMD() {
        System.out.println("Chose a model for AMD processor:\n1.Ryzen 2000\n2.Ryzen 3000\n3.Ryzen 4000");
    }

    public static void monitorBrand() {
        System.out.println("Chose a monitor brand:\n1.LG\n2.Samsung");
    }

    public static void monitorSize() {
        System.out.println("Chose a monitor size in inch:\n1.20\n2.25");
    }

    public static void mouseBrand() {
        System.out.println("Chose a mouse brand:\n1.SteelSeries\n2.Razor");
    }

    public static void isMouseOptical() {
        System.out.println("Optical mouse or not:\n1.Yes\n2.No");
    }

    public static void keyboardBrand () {
        System.out.println("Chose a keyboard brand:\n1.Corsair\n2.HyperX");
    }

    public static void isKeyboardMechanical() {
        System.out.println("Mechanical keyboard or not:\n1.Yes\n2.No");
    }

    public static void showMenu() {
        System.out.println("Chose an option\n1.Create computer\n2.Show your computer\n3.Show price of created computer\n9.End");
    }
}
