public class RAM {
    private String RAMBrand;
    private String RAMModel;
    private int RAMAmount; //in Mb
    private int RAMPrice;

    public RAM() {
    }

    public RAM(String RAMBrand, String RAMModel, int RAMAmount, int RAMPrice) {
        this.RAMBrand = RAMBrand;
        this.RAMAmount = RAMAmount;
        this.RAMModel = RAMModel;
        this.RAMPrice = RAMPrice;
    }

    public String getRAMBrand() {
        return RAMBrand;
    }

    public void setRAMBrand(int option) {
        switch (option) {
            case 1:
                this.RAMBrand = "Corsair";
                break;
            case 2:
                this.RAMBrand = "G.Skill";
                break;
            default:
                System.out.println("Enter a valid option");
                this.RAMBrand = "Invalid";
        }
    }

    public String getRAMModel() {
        return RAMModel;
    }

    public void setRAMModel(int option) {
        if (this.getRAMBrand().equals("Corsair")) {
            switch (option) {
                case 1:
                    this.RAMModel = "Vengeance LED";
                    break;
                case 2:
                    this.RAMModel = "Dominator Platinum RGB";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.RAMModel = "Invalid";
                    break;
            }
        } else if (this.getRAMBrand().equals("G.Skill")) {
            switch (option) {
                case 1:
                    this.RAMModel = "Trident Z RGB";
                    break;
                case 2:
                    this.RAMModel = "Trident Z Royal";
                    break;
                default:
                    System.out.println("Chose a valid option");
                    this.RAMModel = "Invalid";
                    break;
            }
        } else {
            System.out.println("You need to chose a valid socket before you chose a model");
        }
    }

    public int getRAMAmount() {
        return RAMAmount;
    }

    public void setRAMAmount(int option) {
        switch (option) {
            case 1:
                this.RAMAmount = 8192;
                break;
            case 2:
                this.RAMAmount = 16384;
                break;
            case 3:
                this.RAMAmount = 32769;
                break;
            default:
                System.out.println("Chose a valid option");
                this.RAMAmount = 0;
                break;
        }
    }

    public int getRAMPrice() {
        return RAMPrice;
    }

    public void setRAMPrice() {
       if(this.RAMAmount == 8192){
           this.RAMPrice = 300;
       } else if(this.RAMAmount == 16384){
           this.RAMPrice = 400;
       } else if(this.RAMAmount == 32769){
           this.RAMPrice = 550;
       }
    }
}
