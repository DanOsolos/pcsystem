import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        PcCase pcCase = new PcCase();
        Motherboard motherboard = new Motherboard();
        PowerSupply powerSupply = new PowerSupply();
        VideoCard videoCard = new VideoCard();
        Processor processor = new Processor();
        RAM RAM = new RAM();
        Monitor monitor = new Monitor();
        Mouse mouse = new Mouse();
        Keyboard keyboard = new Keyboard();

        System.out.println("Enter a budget for pc creation");
        int budget = scanner.nextInt();

        int option, optionCreate, totalPrice = 0;
        do {
            ShowOptions.showMenu();
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Create a new computer");

                    //Create pc case
                    ShowOptions.pcCaseBrand();
                    optionCreate = scanner.nextInt();
                    pcCase.setCaseBrand(optionCreate);
                    pcCase.setCasePrice();
                    System.out.print("The price of the chosen product is: " + pcCase.getCasePrice() + "\n");
                    totalPrice += totalPrice + pcCase.getCasePrice();

                    //Create motherboard
                    ShowOptions.motherboardBrands();
                    optionCreate = scanner.nextInt();
                    motherboard.setMotherboardBrand(optionCreate);
                    ShowOptions.motherboardSocket();
                    optionCreate = scanner.nextInt();
                    motherboard.setMotherboardSocket(optionCreate);
                    if (motherboard.getMotherboardSocket().equals("Intel")) {
                        ShowOptions.motherboardModelIntel();
                        optionCreate = scanner.nextInt();
                        motherboard.setMotherBoardModel(optionCreate);
                    } else {
                        ShowOptions.motherboardModelAMD();
                        optionCreate = scanner.nextInt();
                        motherboard.setMotherBoardModel(optionCreate);
                    }
                    motherboard.setMotherBordPrice();
                    System.out.print("The price of the chosen product is: " + motherboard.getMotherBordPrice() + "\n");
                    totalPrice += totalPrice + motherboard.getMotherBordPrice();

                    //Create power supply
                    ShowOptions.powerSupplyBrand();
                    optionCreate = scanner.nextInt();
                    powerSupply.setPowerSupplyBrand(optionCreate);
                    ShowOptions.powerSupplyPower();
                    optionCreate = scanner.nextInt();
                    powerSupply.setPowerSupplyPower(optionCreate);
                    if (powerSupply.getPowerSupplyPower() == 750) {
                        ShowOptions.powerSupplyModel750W();
                        optionCreate = scanner.nextInt();
                        powerSupply.setPowerSupplyModel(optionCreate);
                    } else if (powerSupply.getPowerSupplyPower() == 1000) {
                        ShowOptions.powerSupplyModel1000W();
                        optionCreate = scanner.nextInt();
                        powerSupply.setPowerSupplyModel(optionCreate);
                    } else if (powerSupply.getPowerSupplyPower() == 1500) {
                        ShowOptions.powerSupplyModel1500W();
                        optionCreate = scanner.nextInt();
                        powerSupply.setPowerSupplyModel(optionCreate);
                    }
                    powerSupply.setPowerSupplyPrice();
                    System.out.print("The price of the chosen product is: " + powerSupply.getPowerSupplyPrice() + "\n");
                    totalPrice += totalPrice + powerSupply.getPowerSupplyPrice();

                    //Create processor
                    ShowOptions.processorBrand();
                    optionCreate = scanner.nextInt();
                    processor.setProcessorBrand(optionCreate);
                    if (processor.getProcessorBrand().equals("Intel")) {
                        ShowOptions.processorModelIntel();
                        optionCreate = scanner.nextInt();
                        processor.setProcessorModel(optionCreate);
                    } else if (processor.getProcessorBrand().equals("AMD")) {
                        ShowOptions.processorModelAMD();
                        optionCreate = scanner.nextInt();
                        processor.setProcessorModel(optionCreate);
                    }
                    processor.setProcessorPrice();
                    System.out.print("The price of the chosen product is: " + processor.getProcessorPrice() + "\n");
                    totalPrice += processor.getProcessorPrice();

                    //Create video card
                    ShowOptions.videoCardBrand();
                    optionCreate = scanner.nextInt();
                    videoCard.setVideoCardBrand(optionCreate);
                    if (videoCard.getVideoCardBrand().equals("Nvidia")) {
                        ShowOptions.videoCardModelNvidia();
                        optionCreate = scanner.nextInt();
                        videoCard.setVideoCardModel(optionCreate);
                    } else if (videoCard.getVideoCardBrand().equals("AMD")) {
                        ShowOptions.videoCardModelAMD();
                        optionCreate = scanner.nextInt();
                        videoCard.setVideoCardModel(optionCreate);
                    }
                    videoCard.setVideoCardPrice();
                    System.out.print("The price of the chose product is: " + videoCard.getVideoCardPrice() + "\n");
                    totalPrice += videoCard.getVideoCardPrice();

                    //Create RAM
                    ShowOptions.RAMAmount();
                    optionCreate = scanner.nextInt();
                    RAM.setRAMAmount(optionCreate);
                    ShowOptions.RAMBrand();
                    optionCreate = scanner.nextInt();
                    RAM.setRAMBrand(optionCreate);
                    if (RAM.getRAMBrand().equals("Corsair")) {
                        ShowOptions.RAMModelCorsair();
                        optionCreate = scanner.nextInt();
                        RAM.setRAMBrand(optionCreate);
                    } else if (RAM.getRAMBrand().equals("G.Skill")) {
                        ShowOptions.RAMModelGSkill();
                        optionCreate = scanner.nextInt();
                        RAM.setRAMBrand(optionCreate);
                    }
                    RAM.setRAMPrice();
                    System.out.print("The price of the chosen product is: " + RAM.getRAMPrice() + "\n");
                    totalPrice += RAM.getRAMPrice();

                    //Create monitor
                    ShowOptions.monitorBrand();
                    optionCreate = scanner.nextInt();
                    monitor.setMonitorBrand(optionCreate);
                    ShowOptions.monitorSize();
                    optionCreate = scanner.nextInt();
                    monitor.setMonitorSize(optionCreate);
                    monitor.setMonitorPrice();
                    System.out.print("The price of the chosen product is: " + monitor.getMonitorPrice() + "\n");
                    totalPrice += monitor.getMonitorPrice();

                    //Create mouse
                    ShowOptions.mouseBrand();
                    optionCreate = scanner.nextInt();
                    mouse.setMouseBrand(optionCreate);
                    ShowOptions.isMouseOptical();
                    optionCreate = scanner.nextInt();
                    mouse.setOptical(optionCreate);
                    mouse.setMousePrice();
                    System.out.print("The price of the chosen product is: " + mouse.getMousePrice() + "\n");
                    totalPrice += mouse.getMousePrice();

                    //Create keyboard
                    ShowOptions.keyboardBrand();
                    optionCreate = scanner.nextInt();
                    keyboard.setKeyboardBrand(optionCreate);
                    ShowOptions.isKeyboardMechanical();
                    optionCreate = scanner.nextInt();
                    keyboard.setMechanical(optionCreate);
                    keyboard.setKeyboardPrice();
                    System.out.print("The price of the chosen product is: " + keyboard.getKeyboardPrice() + "\n");
                    totalPrice += keyboard.getKeyboardPrice();

                    System.out.print("The total price of the PC is: " + totalPrice + "\n");

                    if (budget - totalPrice < 0) {
                        System.out.println("You went over your budget " + budget);
                        System.out.println("Program terminated");
                        option = 9;
                    }
                    break;
                case 2:
                    System.out.print("The pc has the specs: "
                            + "\nPc case: " + pcCase.getCaseBrand()
                            + "\nPower supply: " + powerSupply.getPowerSupplyBrand() + " power " + powerSupply.getPowerSupplyPower() + "Watts, model " + powerSupply.getPowerSupplyModel()
                            + "\nMotherboard: " + motherboard.getMotherboardBrand() + " ,socket " + motherboard.getMotherboardSocket() + " ,model " + motherboard.getMotherBoardModel()
                            + "\nProcessor: " + processor.getProcessorBrand() + " ,model " + processor.getProcessorModel()
                            + "\nVideo card: " + videoCard.getVideoCardBrand() + " ,model " + videoCard.getVideoCardModel()
                            + "\nRAM: " + RAM.getRAMBrand() + " ,model " + RAM.getRAMModel() + " ,amount " + RAM.getRAMAmount()
                            + "\nDisplay: " + monitor.getMonitorBrand() + " ,size " + monitor.getMonitorSize()
                            + "\nMouse: " + mouse.getMouseBrand() + " ,optical or not " + mouse.isOptical()
                            + "\nKeyboard: " + keyboard.getKeyboardBrand() + " ,mechanical or not " + keyboard.isMechanical() + "\n");
                    break;
                case 3:
                    System.out.print("The price of the case is: " + pcCase.getCasePrice()
                            + "\nThe price of the motherboard is: " + motherboard.getMotherBordPrice()
                            + "\nThe price of the power supply is: " + powerSupply.getPowerSupplyPrice()
                            + "\nThe price of the processor is: " + processor.getProcessorPrice()
                            + "\nThe price of the video card is: " + videoCard.getVideoCardPrice()
                            + "\nThe price of the RAM is: " + RAM.getRAMPrice()
                            + "\nThe price of the monitor is: " + monitor.getMonitorPrice()
                            + "\nThe price of the mouse is: " + mouse.getMousePrice()
                            + "\nThe price of the keyboard is: " + keyboard.getKeyboardPrice() + "\n");
                    break;
                case 9:
                    System.out.println("Program terminated");
                    break;
                default:
                    System.out.println("Enter a valid option");
                    break;
            }
        } while (option != 9);
    }
}
